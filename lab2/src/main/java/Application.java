import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.sql.SQLException;

public class Application {
    public static void main(String[] args) {
        Logger logger = LogManager.getLogger(Application.class);
        if (1 != args.length) {
            logger.info("No argument");
            return;
        }
        System.out.println("Start");
        try (InputStream inputStream = new BZip2CompressorInputStream(new FileInputStream(args[0]))) {
            var counter = new ReaderCounterXML(inputStream);
            counter.run(2);
        } catch (IOException | XMLStreamException | JAXBException | SQLException | URISyntaxException e) {
            logger.error(e);
        }
    }
}
