package dao;

import pack.jaxb.Node;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;

public class NodeDao {
    public NodeDao(Connection connection) throws SQLException {
        preparedStatement = connection.prepareStatement("insert into node (id, username) values (?, ?)");
        statement = connection.createStatement();
    }
    public void insert0(Node node) throws SQLException {
        var sql = "insert into node (id, username) values ("
                + node.getId().longValue()
                + ", '"
                + node.getUser().replace("'", "''")
                + "')";
        statement.execute(sql);
    }
    public void insert1(Node node) throws SQLException {
        preparedStatement.setLong(1, node.getId().longValue());
        preparedStatement.setString(2, node.getUser());
        preparedStatement.execute();
    }
    public void insert2(Collection<Node> aNode) throws SQLException {
        for (var node : aNode) {
            preparedStatement.setLong(1, node.getId().longValue());
            preparedStatement.setString(2, node.getUser());
            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }

    private PreparedStatement preparedStatement;
    private Statement statement;
}
