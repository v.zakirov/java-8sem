package dao;

import pack.jaxb.MyTag;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;

public class TagDao {
    public TagDao(Connection connection) throws SQLException {
        preparedStatement = connection.prepareStatement("insert into tag (k, v, node_id) values (?, ?, ?)");
        statement = connection.createStatement();
    }
    public void insert0(MyTag tag) throws SQLException {
        var sql = "insert into tag (k, v, node_id) values ('"
                + tag.getK().replace("'", "''")
                + "', '"
                + tag.getV().replace("'", "''")
                + "', "
                + tag.getNodeId()
                + ")";
        statement.execute(sql);
    }
    public void insert1(MyTag tag) throws SQLException {
        preparedStatement.setString(1, tag.getK());
        preparedStatement.setString(2, tag.getV());
        preparedStatement.setLong(3, tag.getNodeId());
        preparedStatement.execute();
    }
    public void insert2(Collection<MyTag> aTag) throws SQLException {
        for (var tag : aTag) {
            preparedStatement.setString(1, tag.getK());
            preparedStatement.setString(2, tag.getV());
            preparedStatement.setLong(3, tag.getNodeId());
            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }

    private PreparedStatement preparedStatement;
    private Statement statement;
}
