import pack.jaxb.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class ReaderCounterXML {
    public ReaderCounterXML(InputStream inputStream) throws XMLStreamException {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        this.xmlStreamReader = factory.createXMLStreamReader(inputStream);
    }

    public void run(int inserter) throws XMLStreamException, JAXBException, SQLException, IOException, URISyntaxException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Node.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Connection connection = GetDataBase();
        Service service = new Service(connection);

        long timeStart = System.nanoTime();
        switch (inserter) {
            case 0:
                while (hasNextTagWithName("node")) {
                    ++numNode;
                    Node node = (Node)unmarshaller.unmarshal(xmlStreamReader);
                    service.insert0(node);
                }
                break;

            case 1:
                while (hasNextTagWithName("node")) {
                    ++numNode;
                    Node node = (Node)unmarshaller.unmarshal(xmlStreamReader);
                    service.insert1(node);
                }
                break;
            case 2:
                ArrayList<Node> aNode = new ArrayList<>();
                while (hasNextTagWithName("node")) {
                    ++numNode;
                    Node node = (Node)unmarshaller.unmarshal(xmlStreamReader);
                    aNode.add(node);
                    if (2048 == aNode.size()) {
                        service.insert2(aNode);
                        aNode.clear();
                    }
                }
                service.insert2(aNode);
                break;
            default:
                throw new RuntimeException("No way");
        }
        long timeEnd = System.nanoTime();
        double result = numNode / ((timeEnd - timeStart) / 1e9);
        System.out.println("Time = " + result);
        System.out.println("Time = " + ((timeEnd - timeStart) / 1e9));
    }

    private boolean hasNextTagWithName(String key) throws XMLStreamException {
        while (xmlStreamReader.hasNext()) {
            int eventType = xmlStreamReader.next();
            if (eventType == XMLStreamConstants.START_ELEMENT
             && xmlStreamReader.getLocalName().equals(key)) {
                return true;
            }
        }
        return false;
    }

    public static Connection GetDataBase() throws IOException, SQLException, URISyntaxException {
        Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/zvk", "postgres", "123");
        URL url = Application.class.getClassLoader().getResource("init.sql");
        if (url != null) {
            String string = Files.readString(Path.of(url.toURI()));
            Statement statement = connection.createStatement();
            statement.execute(string);
        }
        return connection;
    }

    private XMLStreamReader xmlStreamReader;
    private long numNode = 0;
}

