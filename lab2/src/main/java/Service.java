import dao.NodeDao;
import dao.TagDao;
import pack.jaxb.MyTag;
import pack.jaxb.Node;
import pack.jaxb.Tag;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.stream.Collectors;

public class Service {
    public Service(Connection connection) throws SQLException {
        nodeDao = new NodeDao(connection);
        tagDao = new TagDao(connection);
    }
    public void insert0(Node node) throws SQLException {
        nodeDao.insert0(node);
        for (Tag tag : node.getTag())
            tagDao.insert0(MyTag.createMyTag(tag, node));
    }
    public void insert1(Node node) throws SQLException {
        nodeDao.insert1(node);
        for (Tag tag : node.getTag())
            tagDao.insert1(MyTag.createMyTag(tag, node));
    }
    public void insert2(Collection<Node> aNode) throws SQLException {
        nodeDao.insert2(aNode);
        tagDao.insert2(aNode.stream().flatMap(node -> node.getTag().stream().map(tag -> MyTag.createMyTag(tag, node))).collect(Collectors.toList()));
    }
    private NodeDao nodeDao;
    private TagDao tagDao;
}
