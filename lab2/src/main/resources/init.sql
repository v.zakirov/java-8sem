DROP TABLE IF EXISTS tag;
DROP TABLE IF EXISTS node;

CREATE TABLE node
(
    id          BIGINT          PRIMARY KEY,
    lat         FLOAT          ,
    lon         FLOAT          ,
    username    VARCHAR(256)    NOT NULL
);

CREATE TABLE tag
(
    id      BIGINT          PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    k       VARCHAR(256)    NOT NULL,
    v       VARCHAR(256)    NOT NULL,
    node_id BIGINT REFERENCES node(id) ON DELETE CASCADE
);
