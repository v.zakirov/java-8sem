package ru.nsu.zvk.lab3.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.nsu.zvk.lab3.pack.jaxb.NodeEntity;

import java.util.List;

public interface NodeRepository extends JpaRepository<NodeEntity, Long> {
    @Query(nativeQuery = true,
    value = "SELECT *, earth_distance(ll_to_earth(?1, ?2), ll_to_earth(node.lat, node.lon)) as dist " +
            "FROM node WHERE earth_distance(ll_to_earth(?1, ?2), ll_to_earth(node.lat, node.lon)) < ?3 ORDER BY dist")
    List<NodeEntity> find(double lat, double lon, double rad);
}
