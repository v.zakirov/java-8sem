package ru.nsu.zvk.lab3.pack.jaxb;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "node")
public class NodeEntity {
    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "lat")
    private Double lat;

    @Column(name = "lon")
    private Double lon;

    @Column(name = "username")
    private String username;

    @OneToMany(mappedBy = "node", cascade = CascadeType.ALL)
    private List<TagEntity> tags;

}
