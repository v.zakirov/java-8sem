package ru.nsu.zvk.lab3;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TagInfo {
    private String k;
    private String v;
}
