package ru.nsu.zvk.lab3;

import org.springframework.web.bind.annotation.*;
import ru.nsu.zvk.lab3.service.NodeService;

import java.util.List;

@RestController
@RequestMapping("/node")
public class Controller {

    public Controller(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    @GetMapping("/get-node")
    public NodeInf getNodeById(@RequestParam long id) {
        return nodeService.findById(id);
    }

    @GetMapping("/get-nodes")
    public List<NodeInf> getNodesByRad(@RequestParam double lat,
                                       @RequestParam double lon,
                                       @RequestParam double rad) {
        return nodeService.find(lat, lon, rad);
    }

    @PostMapping("/post-node")
    public void postNode(@RequestBody NodeInf nodeInf) {
        nodeService.add(nodeInf);
    }

    @DeleteMapping("delete-node")
    public void deleteNode(@RequestParam long id) {
        nodeService.delete(id);
    }



    private NodeService nodeService;
}
