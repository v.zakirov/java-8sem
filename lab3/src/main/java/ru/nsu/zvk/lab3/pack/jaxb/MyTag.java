package ru.nsu.zvk.lab3.pack.jaxb;

public class MyTag {
    public MyTag(String k, String v, long node_id) {
        this.k = k;
        this.v = v;
        this.node_id = node_id;
    }
    public static MyTag createMyTag(Tag tag, Node node) {
        return new MyTag(tag.getK(), tag.getV(), node.getId().longValue());
    }

    public String getK() {return k;}
    public String getV() {return v;}
    public long getNodeId() {return node_id;}

    private String k;
    private String v;
    private long node_id;
}
