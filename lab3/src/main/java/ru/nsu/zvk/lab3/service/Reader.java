package ru.nsu.zvk.lab3.service;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.springframework.stereotype.Service;
import ru.nsu.zvk.lab3.Application;
import ru.nsu.zvk.lab3.pack.jaxb.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

@Service
public class Reader {
    public Reader(NodeService nodeService) {
        this.nodeService = nodeService;
    }
   // @PostConstruct
    private void init() {
        try {
            Connection connection = Reader.GetDataBase();
            XMLInputFactory factory = XMLInputFactory.newInstance();
            InputStream inputStream = new BZip2CompressorInputStream(new FileInputStream("RU-NVS.osm.bz2"));
            xmlStreamReader = factory.createXMLStreamReader(inputStream);
            run();
        } catch (IOException | XMLStreamException | JAXBException | SQLException | URISyntaxException e) {
            e.printStackTrace();
        }
    }


    public void run() throws JAXBException, XMLStreamException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Node.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        while (hasNextTagWithName("node")) {
            nodeService.add((Node)unmarshaller.unmarshal(xmlStreamReader));
        }
    }

    private boolean hasNextTagWithName(String key) throws XMLStreamException {
        while (xmlStreamReader.hasNext()) {
            int eventType = xmlStreamReader.next();
            if (eventType == XMLStreamConstants.START_ELEMENT
                    && xmlStreamReader.getLocalName().equals(key)) {
                return true;
            }
        }
        return false;
    }

    public static Connection GetDataBase() throws IOException, SQLException, URISyntaxException {
        Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/zvk", "postgres", "123");
        URL url = Application.class.getClassLoader().getResource("init.sql");
        if (url != null) {
            String string = Files.readString(Path.of(url.toURI()));
            Statement statement = connection.createStatement();
            statement.execute(string);
        }
        return connection;
    }


    private XMLStreamReader xmlStreamReader;
    private NodeService nodeService;
}
