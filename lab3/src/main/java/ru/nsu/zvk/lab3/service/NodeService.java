package ru.nsu.zvk.lab3.service;

import org.springframework.stereotype.Service;
import ru.nsu.zvk.lab3.Mapper;
import ru.nsu.zvk.lab3.NodeInf;
import ru.nsu.zvk.lab3.pack.jaxb.Node;
import ru.nsu.zvk.lab3.repository.NodeRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class NodeService {
    public NodeService(NodeRepository nodeRepository, Mapper mapper) {
        this.nodeRepository = nodeRepository;
        this.mapper = mapper;
    }

    @Transactional
    public List<NodeInf> find(double lat, double lon, double rad) {
        return nodeRepository.find(lat, lon, rad).stream().map(nodeEntity -> mapper.map(nodeEntity)).collect(Collectors.toList());
    }

    @Transactional
    public NodeInf findById(long id) {
        var res = nodeRepository.findById(id).map(nodeEntity -> mapper.map(nodeEntity));
        return res.orElse(new NodeInf());
    }

    @Transactional
    public void add(NodeInf nodeInf) {
        nodeRepository.save(mapper.map(nodeInf));
    }

    @Transactional
    public void delete(long id) {
        nodeRepository.deleteById(id);
    }


    @Transactional
    public void add(Node node) {
        nodeRepository.save(mapper.map(node));
    }

    private NodeRepository nodeRepository;
    private Mapper mapper;
}
