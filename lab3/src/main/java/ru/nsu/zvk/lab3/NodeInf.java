package ru.nsu.zvk.lab3;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class NodeInf {
    private long id;
    private Double lat;
    private Double lon;
    private String username;
    public List<TagInfo> tags;
}
