package ru.nsu.zvk.lab3.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.zvk.lab3.pack.jaxb.TagEntity;

public interface TagRepository extends JpaRepository<TagEntity, Long> {
}
