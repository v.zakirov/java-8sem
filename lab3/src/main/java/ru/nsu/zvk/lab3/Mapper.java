package ru.nsu.zvk.lab3;

import org.springframework.stereotype.Component;
import ru.nsu.zvk.lab3.pack.jaxb.Node;
import ru.nsu.zvk.lab3.pack.jaxb.NodeEntity;
import ru.nsu.zvk.lab3.pack.jaxb.Tag;
import ru.nsu.zvk.lab3.pack.jaxb.TagEntity;

import java.util.stream.Collectors;

@Component
public class Mapper {
    public NodeEntity map(Node node) {
        NodeEntity nodeEntity = new NodeEntity();
        nodeEntity.setId(node.getId().longValue());
        nodeEntity.setLat(node.getLat());
        nodeEntity.setLon(node.getLon());
        nodeEntity.setUsername(node.getUser());
        nodeEntity.setTags(node.getTag().stream().map(tag -> map(tag, nodeEntity)).collect(Collectors.toList()));
        return nodeEntity;
    }

    public TagEntity map(Tag tag, NodeEntity nodeEntity) {
        TagEntity tagEntity = new TagEntity();
        tagEntity.setK(tag.getK());
        tagEntity.setV(tag.getV());
        tagEntity.setNode(nodeEntity);
        return tagEntity;
    }

    public NodeInf map(NodeEntity nodeEntity) {
        NodeInf nodeInf = new NodeInf();
        nodeInf.setId(nodeEntity.getId());
        nodeInf.setLat(nodeEntity.getLat());
        nodeInf.setLon(nodeEntity.getLon());
        nodeInf.setUsername(nodeEntity.getUsername());
        nodeInf.setTags(nodeEntity.getTags().stream().map(this::map).collect(Collectors.toList()));
        return nodeInf;
    }

    public TagInfo map(TagEntity tagEntity) {
        TagInfo tagInfo = new TagInfo();
        tagInfo.setK(tagEntity.getK());
        tagInfo.setV(tagEntity.getV());
        return tagInfo;
    }

    public NodeEntity map(NodeInf nodeInf) {
        NodeEntity nodeEntity = new NodeEntity();
        nodeEntity.setId(nodeInf.getId());
        nodeEntity.setLat(nodeInf.getLat());
        nodeEntity.setLon(nodeInf.getLon());
        nodeEntity.setUsername(nodeInf.getUsername());
        nodeEntity.setTags(nodeInf.getTags().stream().map(this::map).collect(Collectors.toList()));
        return nodeEntity;
    }

    public TagEntity map(TagInfo tagInfo) {
        TagEntity tagEntity = new TagEntity();
        tagEntity.setK(tagInfo.getK());
        tagEntity.setV(tagInfo.getV());
        return tagEntity;
    }
}
