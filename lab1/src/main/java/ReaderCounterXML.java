import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;
import java.util.*;

public class ReaderCounterXML {
    public ReaderCounterXML(InputStream inputStream) throws XMLStreamException {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        this.xmlStreamReader = factory.createXMLStreamReader(inputStream);
    }

    public void run() throws XMLStreamException {
        HashMap<String, Integer> userToCount = new HashMap<>();
        HashMap<String, Integer> uniqueNameToCount = new HashMap<String, Integer>();
        while (hasNextTagWithName("node")) {
            String user = xmlStreamReader.getAttributeValue(null, "user");
            if (userToCount.containsKey(user)) {
                Integer oldValue = userToCount.get(user);
                userToCount.put(user, oldValue + 1);
            } else {
                userToCount.put(user, 1);
            }

            while (hasNextTagWithName("tag")) {
                String tagKey = xmlStreamReader.getAttributeValue(null, "k");
                if (uniqueNameToCount.containsKey(tagKey)) {
                    Integer oldValue = uniqueNameToCount.get(tagKey);
                    uniqueNameToCount.put(tagKey, oldValue + 1);
                } else {
                    uniqueNameToCount.put(user, 1);
                }
            }
        }

        List<Map.Entry<String, Integer>> list1 = getSortedList(userToCount);
        List<Map.Entry<String, Integer>> list2 = getSortedList(uniqueNameToCount);

        System.out.println("user - count");
        list1.forEach( l -> System.out.println(l.getKey() + " - " + l.getValue()));
        System.out.println("key - count");
        list2.forEach( l -> System.out.println(l.getKey() + " - " + l.getValue()));
    }

    private List<Map.Entry<String, Integer>> getSortedList(HashMap<String, Integer> map) {
        List<Map.Entry<String, Integer>> list = new ArrayList(map.entrySet());
        list.sort(new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return o2.getValue() - o1.getValue();
            }
        });
        return list;
    }

    private boolean hasNextTagWithName(String key) throws XMLStreamException {
        while (xmlStreamReader.hasNext()) {
            int eventType = xmlStreamReader.next();
            if (eventType == XMLStreamConstants.START_ELEMENT
             && xmlStreamReader.getLocalName().equals(key)) {
                return true;
            }
            if (key.equals("tag") && eventType == XMLStreamConstants.END_ELEMENT)
                return false;
        }
        return false;
    }

    private XMLStreamReader xmlStreamReader;
}

